% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/datafuncs.R
\name{clean_owid}
\alias{clean_owid}
\title{Clean OWID directory}
\usage{
clean_owid(owiddatdir = "../../data/owid/")
}
\arguments{
\item{datdir}{The directory containing OWID files}
}
\value{
The date of the retained files
}
\description{
Clean OWID directory
}
\details{
Deletes all but the most recent OWID files
}
