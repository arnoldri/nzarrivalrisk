% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{is.unique}
\alias{is.unique}
\title{Test whether the elements of vector x are unique}
\usage{
is.unique(x)
}
\arguments{
\item{x}{Vector of interest}
}
\value{
Logical value TRUE or FALSE
}
\description{
Test whether the elements of vector x are unique
}
\details{
Vector x can be of any type.  Returns NA if any element of x is NA.
}
\examples{
is.unique(1:3)
is.unique(c(1,1,2))

}
