# NZ COVID19 Arrival Risk

The code in this repository (https://gitlab.com/arnoldri/nzarrivalrisk/) 
is for an R package which *nzarrivalrisk* which implements a
regression and forecasting model for the border risk of COVID19.

It is made available as supporting material for the paper: 

*Estimating the risk of SARS-CoV-2 infection in New Zealand border arrivals*, 
by 
Richard Arnold (Victoria University of Wellington)
Rachelle N. Binny (Manaaki Whenua),
Thomas Lumley (University of Auckland), 
Audrey Lustig (Manaaki Whenua), 
Matthew Parry (University of Otago)
and Michael J. Plank (University of Canterbury), 
submitted to the journal 'BMC Global and Public Health.'

The data that it relies on cannot be shared, because it includes counts of arrivals and 
positive cases by week by country for all passengers arriving in New Zealand.  These data
are too identifying to be released, especially for countries with low numbers of arrivals
and cases.

The code is all visible in the R/ directory and documentation of most of the functions 
is in the man/ directory - easily accessible when the repository is downloaded and built as an 
R package "nzarrivalrisk"

They key steps in data processing are as follows:

 * Reading latest worldwide counts of population and cases in each country by week (from
   Our World in Data)
 * Reading latest counts of arrivals by country from New Zealand Customs
 * Reading latest counts of cases among arrived cases from the New Zealand Ministry of Health
 * The data are then modelled using a call to the function `train_data()` - this outputs an 
   object with various estimates of arrival risk which can be queried and displayed.

The model is specified in the following technical report:

https://www.covid19modelling.ac.nz/estimating-covid-19-border-arrival-risk-in-aotearoa-new-zealand/

and examples of the numerical and graphic output the code creates are given in that report.

The code was written by the authors and is maintained by Richard Arnold (richard.arnold@vuw.ac.nz).
