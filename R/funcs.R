# Functions

#' Read in the raw daily data
#' 
#' @param filename Source filename - in Excel format
#' @param file_version 0=Excel file input; 1=CSV file input
#' @param varlist List object containing the column names in the file
#'                corresponding to the key fields that are needed in the output data
#'                The object contains elements specifying the format of dates, and the units of rates
#'                
#' @return A data frame with the daily data
#' 
#' @export
read_daily_data <- function(filename, 
                            file_version=1,
                            varlist=list(varnames=c(iso3c="iso3c",
                                                    country="country",
                                                    continent="continent",
                                                    date="date",
                                                    numCases="numCases",
                                                    numArr="numArr",
                                                    numEligibleCases="numEligibleCases",
                                                    numEligibleArr="numEligibleArr",
                                                    numVaccCases="numVaccCases",
                                                    numVaccArr="numVaccArr",
                                                    newCaseRate="newCasesPerMil",
                                                    newDeathRate="newDeathsPerMil",
                                                    newTestRate="newTestsPerK",
                                                    vaccRate="vaccPer100",
                                                    fullyVaccRate="fullyVaccPer100",
                                                    positiveRate="positiveRate",
                                                    Reff="Reff",
                                                    Rt.epi="Rt.epi",
                                                    population="population",
                                                    owid="owid"),
                                         formats=c(date.format="%Y-%m-%d"),
                                         units=c(newCaseRate=1e6,
                                                 newDeathRate=1e6,
                                                 newTestRate=1000,
                                                 vaccRate=100,
                                                 fullyVaccRate=100,
                                                 positiveRate=1))
) {
  # read daily data
  if(file_version==0) {
    # Old version
    varlist <- list(varnames=c(country="country",
                               date="date",
                               numCases="numCases",
                               numArr="numArr",
                               newCaseRate="newCasesPerMil",
                               newDeathRate="newDeathsPerMil",
                               newTestRate="newTestsPerK",
                               positiveRate="positiveRate",
                               Reff="Reff"),
                    formats=c(date.format="Excel date"),
                    units=c(newCaseRate=1e6,
                            newDeathRate=1e6,
                            newTestRate=1000,
                            positiveRate=1))
    cat(paste0("Reading excel file ",filename,"\n"))
    if(!file.exists(filename)) stop(paste0("file ",filename," does not exist"))
    ddat <- as.data.frame(readxl::read_excel(filename))
  } else {
    cat(paste0("Reading file ",filename,"\n"))
    if(!file.exists(filename)) stop(paste0("file ",filename," does not exist"))
    ddat <- read.csv(filename)
  }
  
  # check variable names are as expected
  if(!all(varlist$varnames %in% names(ddat))) {
    stop("Variable names in the data frame do not match the expected names (in varlist$varnames)")
  }
  
  # rename variables
  m <- match(varlist$varnames, names(ddat))
  names(ddat)[m] <- names(varlist$varnames)
  
  # convert counts per XX population to counts per head of population
  for(vname in names(varlist$units)) {
    ddat[,vname] <- ddat[,vname]/varlist$units[vname]
  }
  
  ## store date in R date format
  if(file_version==0 || varlist$formats["date.format"]=="Excel date") {
    ddat$date <- as.Date(ddat$date)
  } else {
    ddat$date <- as.Date(ddat$date, format=varlist$formats["date.format"])
  }
  ddat$week <- as.week(ddat$date)
  
  # return data, ordered by country and date
  outvarnames <- names(varlist$varnames)
  outvarnames <- c("week",outvarnames)
  ddat <- ddat[order(ddat$country,ddat$date),outvarnames]
  return(ddat)
}

#' Read in the raw daily expected future arrivals data
#' 
#' @param filename Source filename - in csv format
#' @param varlist List object containing the column names in the file
#'                corresponding to the key fields that are needed in the output data
#'                
#' @return A data frame with the daily expected future arrivals data
#' 
#' @export
read_daily_numArrFuture_data <- function(filename, 
                                varlist=list(varnames=list(country="country",
                                                           date="date",
                                                           numArrFuture="numArr"),
                                             formats=list(date.format="%Y-%m-%d"))
                                ) {
  # read daily data
  dfuture <- read.csv(filename)
  
  # check variable names are as expected
  if(!all(varlist$varnames %in% names(dfuture))) {
    stop("Variable names in the data frame do not match the expected names (in varlist$varnames)")
  }
  
  # rename variables
  m <- match(varlist$varnames, names(dfuture))
  names(dfuture)[m] <- names(varlist$varnames)
  
  ## store date in R date format
  dfuture$date <- as.Date(dfuture$date, format=varlist$format$date.format)
  dfuture$week <- as.week(dfuture$date)
  
  # return data, ordered by country and date
  outvarnames <- names(varlist$varnames)
  outvarnames <- c("week",outvarnames)
  dfuture <- dfuture[order(dfuture$country,dfuture$date),outvarnames]
  return(dfuture)
}


#' Daily to Weekly
#' 
#' @param ddat Daily data frame
#' 
#' @return A weekly data frame
#' 
#' @details Convert a daily data object to weekly data
#' 
#' @export
daily_to_weekly <- function(ddat) {
  # convert daily data to weekly
  wdat <- ddat %>% 
    mutate(week=as.week(date)) %>%
    group_by(country_week = paste(country, week, sep="|")) %>% 
    summarise(iso3c=unique(iso3c),
              country=unique(country),
              continent=unique(continent),
              week=unique(week),
              ndays=n(),
              numCases=sum(numCases, na.rm=TRUE),
              numArr=sum(numArr, na.rm=TRUE),
              newCaseRate=7*mean(newCaseRate, na.rm=TRUE),
              newDeathRate=7*mean(newDeathRate, na.rm=TRUE),
              newTestRate=7*mean(newTestRate, na.rm=TRUE),
              vaccRate=max_na(vaccRate, na.rm=TRUE),
              fullyVaccRate=max_na(fullyVaccRate, na.rm=TRUE),
              positiveRate=median(positiveRate, na.rm=TRUE),
              Reff=median(Reff, na.rm=TRUE),
              Rt.epi=median(Rt.epi, na.rm=TRUE),
              population=min(population),
              owid=all(owid),
              date=min_na(date, na.rm=TRUE)) %>%
    ungroup() %>%
    mutate(country_week=NULL)
  wdat <- as.data.frame(wdat)
  wdat$newCaseRate[is.nan(wdat$newCaseRate)] <- NA
  wdat$newDeathRate[is.nan(wdat$newDeathRate)] <- NA
  wdat$newTestRate[is.nan(wdat$newTestRate)] <- NA
  wdat$actual <- TRUE
  wdat <- wdat[order(wdat$country,wdat$week),]
  # flag that these rows are all actual data
  wdat$actual <- TRUE
  return(wdat)
}

#' Aggregate daily future arrivals data to weekly data
#' 
#' @param dfuture Daily future arrivals data set 
#' - as output by read_daily_numArrFuture_data()
#' 
#' @return Weekly future arrivals data set
#' 
#' @export
numArrFuture_daily_to_weekly <- function(dfuture) {
  # convert daily future arrivals data to weekly
  dfuture <- dfuture[!is.na(dfuture$numArr),]
  wfuture <- dfuture %>% 
    mutate(week=as.week(date)) %>%
    group_by(country_week = paste(country, week, sep="|")) %>% 
    summarise(country=unique(country),
              week=unique(week),
              numArrFuture=sum(numArrFuture, na.rm=TRUE)) %>%
    ungroup() %>%
    mutate(country_week=NULL)
  wfuture <- as.data.frame(wfuture)
  wfuture <- wfuture[order(wfuture$country,wfuture$week),]
  return(wfuture)
}

#' Clean the weekly data for analysis
#' 
#' @param wdat Weekly data set as output by daily_to_weekly()
#' @param nwindow Length of window (in weeks) to test for thresholds for minimum numbers 
#' of arrivals and cases
#' @param minNumArr Minimum number of arrivals for a country to be used in modelling
#' @param minNumCases Number of cases for a country to be used in modelling
#' @param verbose Report what data are being excluded at each step
#' 
#' @details Performs a number of cleaning operations
#' \itemize{
#'   \item{Remove any rows where the country name is 'NULL'}
#'   \item{Set to zero any negative counts of cases, deaths or tests}
#'   \item{Where the number of cases > number of arrivals, set the number of cases to zero}
#'   \item{Where the number of cases is missing, set it to zero}
#'   \item{Remove all countries where the total number of arrivals is less than \code{minNumArr}}
#'   \item{Remove all countries where the total number of cases is less than \code{minNumArr}}
#'   \item{Remove all weeks where the in-country incidence is missing (NA)}
#'   \item{Remove all countries where there are any missing weeks (i.e. retain
#'         only those countries where there is a single unbroken sequence of weeks.)}
#'   \item{Truncate the dataset if there are any weeks at the end of the time
#'         period containing no arrivals}
#'   \item{Truncate the final week if the number of days of data is less than 5}
#'   \item{Eliminate any countries with no data in the final week}
#' }
#' Note: this process can leave some weeks in the main body of the data set with less
#' than 7 full days.  We may need to do something about that.
#' 
#' @return Cleaned weekly data set - with an extra column 'retain' which is TRUE
#' if the data for that country can be included in modelling
#' 
#' @export
clean_weekly_data <- function(wdat, nwindow=30, minNumArr=1, minNumCases=0, verbose=TRUE) {
  
  # Flag (set at country level) indicating whether a country can
  # be included in the modelling
  wdat$retain <- TRUE
  # The 'Unknown Origin' country cannot be used in modelling
  wdat$retain[wdat$iso3c=="???"] <- FALSE
  
  # Remove any countries where the name is NULL
  if(verbose) {
    cat(paste0(" - Removing ",length(wdat$country[wdat$country=="NULL"]),
               " row(s) where the country name is NULL\n"))
  }
  wdat <- wdat[!(wdat$country%in%c("NULL")),]
  
  # Set to zero all arrivals and cases attached to New Zealand
  wdat$numCases[wdat$iso3c=="NZL"] <- 0
  wdat$numArr[wdat$iso3c=="NZL"] <- 0
  # This will keep NZ in the dataset, but it won't be used in modelling
  
  # Eliminate (set to 0) any negative counts of cases, deaths or tests
  for(varname in c("newCaseRate","newDeathRate","newTestRate")) {
    idx <- !is.na(wdat[,varname]) & wdat[,varname]<0
    if(verbose) {
      cat(paste0("Setting ",sum(idx)," instances of negative ",varname," to zero.\n"))
    }
    wdat[idx,varname] <- 0
    #wdat[,varname] <- pmax(0, wdat[,varname], na.rm=TRUE)
  }
  
  # Set to zero any counts of cases that are missing
  idx <- is.na(wdat$numCases)
  if(verbose) {
    cat(paste0(" - Setting to zero ",sum(idx)," instances of missing numbers of cases\n"))
  }
  wdat$numCases[idx] <- 0
  
  # Ensure cases where the number of cases > number of arrivals
  # have all cases removed
  idx <- wdat$numCases>wdat$numArr
  if(verbose) {
    cat(paste0(" - Setting ",length(wdat$country[idx]),
               " instances of numCases>numArr to zero cases\n"))
    print(wdat[idx,c("country","week","numCases","numArr")])
  }
  wdat$numCases[idx] <- 0
  
  # Count arrivals and cases by country in the window
  wmax <- max(wdat$week)
  wwindow <- wmax-nwindow+1
  idx <- wdat$week>=wwindow
  totarr <- tapply(wdat$numArr[idx], wdat$country[idx], sum)
  totcases <- tapply(wdat$numCases[idx], wdat$country[idx], sum)
  # Remove countries which had less than minNumArr arrivals and less than minNumCases cases
  wdat$retain <- ( wdat$retain 
                   & (wdat$country%in%names(totarr)[totarr>=minNumArr])
                   & (wdat$country%in%names(totcases)[totcases>=minNumCases])
  )
  if(verbose) {
    cat(" - Removing the following countries because they have total arrivals < ",minNumArr,",in the dataset:\n")
    cat("   ",paste0(names(totarr)[totarr<=minNumArr], collapse="; "))
    cat("\n")
    cat(" - Removing the following countries because they have total cases < ",minNumCases,",in the dataset:\n")
    cat("   ",paste0(names(totcases)[totcases<=minNumCases], collapse="; "))
    cat("\n")
  }
  
  # remove any rows where the incidence is unknown
  idx <- !is.na(wdat$newCaseRate)
  if(verbose) {
    cat(paste0(" - Removing ",length(wdat$country[!idx]),
               " instances where the in country incidence is missing\n"))
    cat("    This occurs in the following countries:\n")
    cat(paste0(sort(unique(wdat$country[!idx])),sep="; "))
    cat("\n")
    cat("    This occurs in the following weeks:\n")
    cat(paste0(sort(unique(wdat$week[!idx])),sep="; "))
    cat("\n")
  }
  wdat <- wdat[idx,]
  
  # retain countries with complete data (no empty weeks)
  nogaps <- tapply(wdat$week, wdat$country, function(x) diff(range(x))==(length(x)-1))
  idx <- (wdat$country %in% names(nogaps[nogaps]))
  wdat$retain <- wdat$retain & idx
  if(verbose) {
    cat(paste0(" - Removing the following countries because the data are not continuous:\n"))
    cat(paste0(sort(unique(wdat$country[!idx])),sep="; "))
    cat("\n")
  }
  wdat <- wdat[order(wdat$country,wdat$week),]
  
  # delete any final weeks where there are no arrivals at all
  ta <- tapply(wdat$numArr, wdat$week, sum)
  ca <- cumsum(rev(ta))
  wobsmax <- as.numeric(names(ca[ca>0][1]))
  if(verbose) {
    removeweeks <- sort(unique(wdat$week[wdat$week>wobsmax]))
    cat(paste0(" - Removing the following ",length(removeweeks),
               " weeks at the end of the dataset because they contain no arrivals:\n"))
    cat(paste0(removeweeks,sep="; "))
    sort(unique(wdat$week[wdat$week>wobsmax]))
    cat("\n")
  }
  wdat <- wdat[wdat$week<=wobsmax,]
  
  # require the final week to have 5 or more days of data on average
  tt <- table(wdat$week, wdat$ndays)
  tw <- apply(tt, 1, which.max)
  wfull <- max(as.numeric(names(tw)[tw>=5]))
  if(verbose) {
    if(wfull<wobsmax) {
      cat(paste0(" - Removing weeks ",wfull+1,"-",wobsmax," due to not having 5 or more complete days\n"))
    }
  }
  wdat <- wdat[wdat$week<=wfull,]
  wobsmax <- wfull
  
  # Eliminate any countries which have no data in the final week
  tf <- tapply(wdat$week, wdat$country, max)
  pcountries <- names(tf[tf<wobsmax])
  if(verbose) {
    cat(paste0(" - Removing the following countries because they have no data in the final retained week:\n"))
    cat(paste0(pcountries,sep="; "))
    cat("\n")
  }
  wdat$retain <- wdat$retain & !(wdat$country%in%pcountries)
  
  # Note: this process can leave some weeks in the main body of the data set with less
  # than 7 full days.  We may need to do something about that.
  
  return(wdat)
}

#' Forecast covariates
#' 
#' @param wdat Weekly data
#' @param nfit Number of weeks to use in the forecast
#' @param nstep Number of weeks to forecast
#' @param caseRateOffset Small offset to be added to case rates to avoid 0
#' @param deathRateOffset Small offset to be added to death rates to avoid 0
#' 
#' @details Forecast the incidence and numbers of arrivals
#' 
#' @return Extended weekly data
#' 
#' @export
forecast_weekly_data <- function(wdat, nfit=3, nstep=12, wfuture=NULL,
                                 caseRateOffset=1e-7, deathRateOffset=1e-7) {

  if(nstep>0) {
    wdat <- do.call(rbind, 
                    by(wdat, wdat$country,
                       forecast_weekly_data_country,
                       nfit=nfit, nstep=nstep, caseRateOffset=caseRateOffset))
  }
  
  if(!is.null(wfuture)) {
    # Overwrite the number of arrivals data with estimated data, where available
    wdat <- merge_numArrFuture_data(wdat, wfuture) 
  }
  # Ensure that NZ is 0
  wdat$numArr[wdat$iso3c=="NZL"] <- 0
  
  return(wdat)
}

#' Forecast covariates for one country only
#' 
#' @param wdat Weekly data
#' @param nfit Number of weeks to use in the forecast
#' @param nstep Number of weeks to forecast
#' @param caseRateOffset Small offset to be added to case rates to avoid 0
#' @param deathRateOffset Small offset to be added to death rates to avoid 0
#' 
#' @details Forecast the incidence and numbers of arrivals
#' 
#' @return Extended weekly data
#' 
#' @export
forecast_weekly_data_country <- function(wsub, nfit=3, nstep=8, 
                                         caseRateOffset=1e-7, deathRateOffset=1e-7) {
  wsub <- wsub[order(wsub$week),]
  wsub$forecast <- FALSE
  imax <- nrow(wsub)
  imin <- max(1,imax-nfit+1)
  nfit <- min(nfit,imax-imin+1)
  wmax <- max(wsub$week)
  
  wnew <- wsub[rep(1,nstep),]
  wnew$week <- wmax + 1:nstep
  wnew$numCases <- NA
  wnew$numArr <- mean(wsub$numArr[imin:imax])
  wnew$newCaseRate <- pmin(1-caseRateOffset,
                           expit(forecast_vector(logit(caseRateOffset+wsub$newCaseRate),
                                          nfit=nfit, nstep=nstep)$yfit))
  wnew$newCaseRate <- pmax(0, wnew$newCaseRate-caseRateOffset)
  wnew$newDeathRate <- pmin(1-deathRateOffset,
                            expit(forecast_vector(logit(deathRateOffset+wsub$newDeathRate),
                                           nfit=nfit, nstep=nstep)$yfit))
  wnew$newDeathRate <- pmax(0, wnew$newDeathRate-deathRateOffset)
  wnew$newTestRate <- NA
  wnew$positiveRate <- NA
  wnew$Reff <- NA #wsub$Reff[imax]
  wnew$date <- week.date(wnew$week)
  wnew$actual <- FALSE
  wnew$forecast <- TRUE
  return(rbind(wsub,wnew))
}    

#' Add future arrivals data
#' 
#' @param wdat Weekly dataset
#' @param wfuture Weekly future arrivals dataset 
#' 
#' @details Update the numbers of arrivals.
#' The future arrivals dataset \code{wfuture} contains
#' \itemize{
#'   \item{country}{Country name}
#'   \item{week}{Week number}
#'   \item{numArr.future}{Number of expected future arrivals}
#' }
#' 
#' @return Updated weekly dataset
#' 
#' @export
merge_numArrFuture_data <- function(wdat, wfuture=NULL) {

  if(is.null(wfuture)) return(wdat)
  
  #if(ncol(wfuture)!=3 || !all(sort(names(wfuture))==c("country","week","numArrFuture"))) {
  #  stop("Data frame of future data is not well formed: should have 3 columns: country/week/numArrFuture")
  #}
  
  wdat$numArrFuture <- NULL
  wdat <- merge(wdat, wfuture, all.x=TRUE)
  wdat$Future <- !wdat$actual & !is.na(wdat$numArrFuture)
  wdat$numArr[wdat$Future] <- wdat$numArrFuture[wdat$Future]
  
  return(wdat)
}

#' Create lagged dataset
#' 
#' @param wdat Weekly data set, as output by daily_to_weekly() or clean_weekly_data()
#' @param lagmax Number of weeks to lag the covariates
#' @param vnames Names of covariates to lag
#' 
#' @details Adds the column 'seq' which the rows from 1 to the number of rows within each country.
#' Adds the column 'weekfactor" as a factor version of the week column.
#' 
#' @return Weekly data set with additional columns containing lagged data
#' 
#' @export
create_lagged_dataset <- function(wdat, 
                                  lagmax=2,
                                  vnames=c("newCaseRate","newDeathRate")) {
  
  # lag the covariates up to lagmax
  
  # create a factor version of the week column
  wdat$weekfactor <- factor(wdat$week)
  # create a sequence number within each country
  wdat$seq <- unlist(tapply(wdat$country, wdat$country, function(x) 1:length(x)))
  
  # make the lagged covariates
  n <- nrow(wdat)
  for(vname in vnames) {
    wdat[,paste0(vname,".0")] <- wdat[,vname]
    for(lag in 1:lagmax) {
       wdat[,paste0(vname,".",lag)] <- c(rep(NA,lag),wdat[1:(n-lag),vname])
       wdat[wdat$seq%in%c(1:lag),paste0(vname,".",lag)] <- NA
    }
  }
  
  return(wdat)
}

#' Add the "id" unique column to the weekly dataset
#' 
#' @param wdat Weekly data
#' 
#' @details Adds a numeric identifier to each row
#' and names the rows using this.  This is needed when
#' merging predictions back on to the data set later
#' 
#' @return Updated weekly data
#' 
#' @export
add_id_column <- function(wdat) {
  wdat <- wdat[order(wdat$country,wdat$week),]
  wdat$id <- 1:nrow(wdat)
  row.names(wdat) <- 1:nrow(wdat)
  return(wdat)
}

#' Fitting the model
#' 
#' @param wdat Weekly data set
#' @param nweeks Number of weeks to fit (see Details)
#' @param model Specification of the model to fit
#' @param subset Logical vector to be used to subset the Weekly data set
#' 
#' @details Currently the function only fits models of type "TMB".
#' 
#' For TMB models the model object must contain the components
#' \itemize{
#'   \item{model}{value "TMB"}
#'   \item{lagmin}{>=0 minimum lag for the covariates}
#'   \item{laxmax}{>=lagmin maximum lag for the covariates}
#'   \item{caseRateOffset}{A small rate value to be added to all 
#'         incidence rates to avoid zero values}
#' }
#' The output object for TMB models contains the elements of 
#' a glmmTMB model fit, with the model specification object,
#' the vector subset, the value nweeks and the model formula
#' stored as a character string.
#' 
#' @return A list object with the details of the fit.  Its structure depends on the 
#' choice of model
#' 
#' @export
fit_model <- function(wdat, 
                      model=list(model="TMB", lagmin=0, lagmax=2, caseRateOffset=1e-7),
                      nweeks=NULL, subset=NULL) {

  if(is.null(subset)) {
    subset <- rep(TRUE,nrow(wdat))
  }
  if(!is.null(nweeks)) {
    wmin <- min(wdat$week[wdat$actual])
    wmax <- max(wdat$week[wdat$actual])
    wmin <- max(wmin, wmax-nweeks+1)
    subset <- subset & (wdat$week>=wmin)
  }

  if(model$model=="OTMB") {
    # Fit TMB model - fit using only actual data
    subset <- subset & wdat$actual
    fmodel <- fit_model_TMB(wdat, model=model, nweeks=nweeks, subset=subset)
  } else if(model$model=="TMB") {
    # Fit TMB model but with forecasts/interpolation by setting numArr=numCases=0
    wdat$numArr[!wdat$actual] <- 0
    wdat$numCases[!wdat$actual] <- 0
    fmodel <- fit_model_TMB(wdat, model=model, nweeks=nweeks, subset=subset)
  } else {
    stop(paste0("Specified model ",model$model," not recognised."))
  }
  
  return(fmodel)
}

#' Add fitted values to the data
#' 
#' @param wdat Weekly data
#' @param Fitted model object
#' @param week.par list objects defining the weeks used in the fit
#' 
#' @return Updated weekly data object with additional columns fit and se.fit (on the link scale)
#' May add additional columns depending on the model (TMB adds random effects u, v)
#' 
#' @export
add_fit <- function(wdat, fmodel, week.par) {
  
  if(fmodel$model=="OTMB") {
    wdat.fit <- add_fit_TMB(wdat, fmodel, week.par, se.fit=TRUE)
  } else if(fmodel$model=="TMB") {
    wdat.fit <- add_fit_TMB(wdat, fmodel, week.par, se.fit=TRUE)
  } else {
    stop("Model not recognised")
  }
  attr(wdat.fit,"model") <- fmodel$model
  
  return(wdat.fit)
}

#' Add forecast values to the data
#' 
#' @param wdat.fit Weekly data with fitted model information
#' @param Fitted model object
#' 
#' @return Updated weekly data object with forecast cases and rates
#' 
#' @details Does nothing if the model is TMB: the fit and the
#' standard errors are already estimated.
#' This routine was designed for the older method OTMB with approximate
#' standard errors
#' 
#' @export
add_forecast <- function(wdat.fit, fmodel) {
  
  if(fmodel$model=="OTMB") {
    # Old version of fitting model with approximate SE
    wdat.fit <- add_forecast_TMB(wdat.fit, fmodel)
  } else if(fmodel$model=="TMB") {
    # no need to do anything here
    invisible()
  } else {
    stop("Model not recognised")
  }
  attr(wdat.fit,"model") <- fmodel$model
  
  return(wdat.fit)
}

#' Compute observed and expected values
#' 
#' @param wdat.fit Fitted model object
#' @param model Type of model (currently only TMB)
#' @param nwsum Number of weeks to sum over
#' @param upper.conf Upper tail probability outside the confidence interval
#' @param upper.pred Upper tail probability outside the prediction interval
#' @param cutpoints A list object with 6 entries: one for the point estimate
#' of mean weekly case numbers, one for mean weekly rates, one for the 
#' the upper bound of the confidence interval of cases, and the equivalent
#' set for the confidence interval of the rates, and finally a set for each
#' of the upper bounds of the prediction intervals.  The object also
#' includes the scalar \code{rate.multliplier} whichi is the denominator for 
#' rates when specifying cutpoints
#' 
#' @details 
#' Compute observed and expected values over an interval
#' Using a set of cut points classify countries into risk bands
#' according to the fitted value, and an upper bound from the
#' confidence and prediction intervals around the fit.
#' 
#' wdat.fit must contain the columns 
#' \itemize{
#'   \item{country}{Country}
#'   \item{week}{Week number}
#'   \item{numArr}{Number of arrivals}
#'   \item{numCases}{Number of cases}
#'   \item{fit}{Fitted value on the link scale}
#'   \item{se.fit}{Standard error of the fit on the link scale}
#' }
#' 
#' @export
compute_obsexp <- function(wdat.fit,
                           nwsum=8, upper.conf=0.1, upper.pred=0.1,
                           cutpoints=list(cases=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                          rates=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                          cases.conf=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                          rates.conf=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf),
                                          cases.pred=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                          rates.pred=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf),
                                          rate.multiplier=1000)
                           ) {
  # upper = area in the upper tail outside the confidence interval
  model <- attributes(wdat.fit)$model
  wmax <- max(wdat.fit$week[wdat.fit$actual])
  wmin <- wmax-nwsum+1
  wsub <- wdat.fit[wdat.fit$week>=wmin & wdat.fit$week<=wmax,]
  zval.conf <- qnorm(1-upper.conf)
  zval.pred <- qnorm(1-upper.pred)
  if(model=="TMB") {
     obsexp <- do.call(rbind, by(wsub, wsub$country,
                              function(wsubsub) {
                                muvec <- expit(wsubsub$fit)
                                binvarvec <- wsubsub$numArr*muvec*(1-muvec)
                                
                                numCases <- sum(wsubsub$numCases)
                                numArr <- sum(wsubsub$numArr)
                                numCasesFit <- sum(wsubsub$numArr*muvec)
                                rateFit <- numCasesFit/numArr
                                vv <- numArr*rateFit*(1-rateFit)
                                
                                se.fit <- sqrt(sum(binvarvec^2*wsubsub$se.fit^2)/vv^2)
                                se.pred <- sqrt(sum(binvarvec^2*wsubsub$se.fit^2 + binvarvec)/vv^2)
                                
                                oesub <- data.frame(
                                  country=wsubsub$country[1],
                                  numCases=numCases,
                                  numArr=numArr,
                                  numCasesFit=numCasesFit,
                                  numCasesFit.upper.conf=numArr*expit(logit(rateFit)+zval.conf*se.fit),
                                  numCasesFit.upper.pred=numArr*expit(logit(rateFit)+zval.pred*se.pred),
                                  rate=numCases/numArr,
                                  rateFit=rateFit,
                                  rateFit.upper.conf=expit(logit(rateFit)+zval.conf*se.fit),
                                  rateFit.upper.pred=expit(logit(rateFit)+zval.pred*se.pred)
                                )
                                return(oesub)
                              }))
  } else {
     stop("Model not recognised")
  }
  
  # classify
  obsexp$cases.class <- set.classes(obsexp$numCasesFit, cutpoints$cases)
  obsexp$rates.class <- set.classes(obsexp$rateFit, 
                                    cutpoints$rate.multiplier*cutpoints$rates)
  
  obsexp$cases.conf.class <- set.classes(obsexp$numCasesFit.upper.conf, cutpoints$cases.conf)
  obsexp$rates.conf.class <- set.classes(obsexp$rateFit.upper.conf, 
                                         cutpoints$rate.multiplier*cutpoints$rates.conf)
  
  obsexp$cases.pred.class <- set.classes(obsexp$numCasesFit.upper.pred, cutpoints$cases.pred)
  obsexp$rates.pred.class <- set.classes(obsexp$rateFit.upper.pred, 
                                         cutpoints$rate.multiplier*cutpoints$rates.pred)

  attr(obsexp,"nwsum") <- nwsum
  attr(obsexp,"cutpoints") <- cutpoints
  return(obsexp)
}



#' Classify countries week by week
#' 
#' @param wdat.fit Fitted model object
#' @param model Type of model (currently only TMB)
#' @param upper.conf Upper tail probability outside the confidence interval
#' @param upper.pred Upper tail probability outside the prediction interval
#' @param cutpoints A list object with 6 entries: one for the point estimate
#' of mean weekly case numbers, one for mean weekly rates, one for the 
#' the upper bound of the confidence interval of cases, and the equivalent
#' set for the confidence interval of the rates, and finally a set for each
#' of the upper bounds of the prediction intervals.  The object also
#' includes the scalar \code{rate.multliplier} whichi is the denominator for 
#' rates when specifying cutpoints
#' 
#' @details 
#' Classify countries into risk bands
#' according to the fitted value, and an upper bound from the
#' confidence and prediction intervals around the fit.
#' 
#' wdat.fit must contain the columns 
#' \itemize{
#'   \item{country}{Country}
#'   \item{week}{Week number}
#'   \item{numArr}{Number of arrivals}
#'   \item{numCases}{Number of cases}
#'   \item{fit}{Fitted value on the link scale}
#'   \item{se.fit}{Standard error of the fit on the link scale}
#' }
#' 
#' @export
classify_countries <- function(wdat.fit, upper.conf=0.1, upper.pred=0.1,
                               cutpoints=list(cases=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                              rates=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                              cases.conf=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                              rates.conf=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf),
                                              cases.pred=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                              rates.pred=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf),
                                              rate.multiplier=1000)
                                 ) {
  model <- attributes(wdat.fit)$model
  zval.conf <- qnorm(1-upper.conf)
  zval.pred <- qnorm(1-upper.conf)
  if(model=="TMB") {
    wdat.class <- wdat.fit
    wdat.class$rate <- wdat.class$numCases/wdat.class$numArr
    wdat.class$rateFit <- expit(wdat.class$fit)
    wdat.class$numCasesFit <- wdat.class$numArr*wdat.class$rateFit
    binvar <- wdat.class$numArr*wdat.class$rateFit*(1-wdat.class$rateFit)
    wdat.class$se.pred <- sqrt(wdat.class$se.fit^2 + 1/binvar)
    wdat.class$rateFit.upper.conf=expit(wdat.class$fit+zval.conf*wdat.class$se.fit)
    wdat.class$rateFit.upper.pred=expit(wdat.class$fit+zval.pred*wdat.class$se.pred)
    wdat.class$numCasesFit.upper.conf=wdat.class$numArr*wdat.class$rateFit.upper.conf
    wdat.class$numCasesFit.upper.pred=wdat.class$numArr*wdat.class$rateFit.upper.pred
  } else {
    stop("Model not recognised")
  }
  
  # classify
  wdat.class$cases.class <- set.classes(wdat.class$numCasesFit, cutpoints$cases)
  wdat.class$rates.class <- set.classes(wdat.class$rateFit, 
                                        cutpoints$rates/cutpoints$rate.multiplier)
  
  wdat.class$cases.conf.class <- set.classes(wdat.class$numCasesFit.upper.conf, cutpoints$cases.conf)
  wdat.class$rates.conf.class <- set.classes(wdat.class$rateFit.upper.conf, 
                                             cutpoints$rates.conf/cutpoints$rate.multiplier)
  
  wdat.class$cases.pred.class <- set.classes(wdat.class$numCasesFit.upper.pred, cutpoints$cases.pred)
  wdat.class$rates.pred.class <- set.classes(wdat.class$rateFit.upper.pred, 
                                             cutpoints$rates.pred/cutpoints$rate.multiplier)
  
  attr(wdat.class,"cutpoints") <- cutpoints
  attr(wdat.class,"upper") <- list(conf=upper.conf, pred=upper.pred)
  return(wdat.class)
}

#' Aggregate over countries
#' 
#' @param wdat.fit Weekly data set with fitted values for each country
#' 
#' @return Weekly data set, aggregated over country
#' 
#' @export
aggregate_over_countries <- function(wdat.fit, caseRateOffset=1e-7) {
  
  wdat.fit$estRate <- expit(wdat.fit$fit)
  wdat.fit$estCases <- wdat.fit$numArr*wdat.fit$estRate
  wdat.fit$var.estCases <- wdat.fit$numArr*wdat.fit$estRate*(1-wdat.fit$estRate)
  wdat.fit$v2v <- wdat.fit$var.estCases^2*wdat.fit$se.fit^2
  
  wdat.fit$estRate.1step <- expit(wdat.fit$fit.1step)
  wdat.fit$estCases.1step <- wdat.fit$numArr*wdat.fit$estRate.1step
  wdat.fit$var.estCases.1step <- wdat.fit$numArr*wdat.fit$estRate.1step*(1-wdat.fit$estRate.1step)
  wdat.fit$v2v.1step <- wdat.fit$var.estCases.1step^2*wdat.fit$se.fit.1step^2
  
  wdat.agg <- wdat.fit %>% 
    group_by(week) %>% 
    summarise(week=unique(week),
              numCases=sum(numCases, na.rm=TRUE),
              numArr=sum(numArr, na.rm=TRUE),
              estCases=sum(estCases, na.rm=TRUE),
              estCases.1step=sum(estCases.1step, na.rm=TRUE),
              actual=mean(actual),
              varCases.conf=sum(v2v, na.rm=TRUE),
              varCases.pred=sum(v2v + var.estCases, na.rm=TRUE),
              varCases.conf.1step=sum(v2v.1step, na.rm=TRUE),
              varCases.pred.1step=sum(v2v.1step + var.estCases.1step, na.rm=TRUE)
    ) %>%
    ungroup() 
  wdat.agg <- as.data.frame(wdat.agg)
  wdat.agg$date <- week.date(wdat.agg$week)
  wdat.agg$actual <- (wdat.agg$actual==1)
  
  wdat.agg$estRate <- wdat.agg$estCases/wdat.agg$numArr
  wdat.agg$fit <- logit(caseRateOffset + wdat.agg$estRate)
  vvec <- wdat.agg$numArr*wdat.agg$estRate*(1-wdat.agg$estRate)
  wdat.agg$se.conf <- sqrt( wdat.agg$varCases.conf/vvec^2 )
  wdat.agg$se.pred <- sqrt( wdat.agg$varCases.pred/vvec^2 )
  wdat.agg$fit[is.na(wdat.agg$se.conf)] <- NA
  wdat.agg$estRate[is.na(wdat.agg$se.conf)] <- NA
  
  wdat.agg$estRate.1step <- wdat.agg$estCases.1step/wdat.agg$numArr
  wdat.agg$fit.1step <- logit(caseRateOffset + wdat.agg$estRate.1step)
  vvec.1step <- wdat.agg$numArr*wdat.agg$estRate.1step*(1-wdat.agg$estRate.1step)
  wdat.agg$se.conf.1step <- sqrt( wdat.agg$varCases.conf.1step/vvec.1step^2 )
  wdat.agg$se.pred.1step <- sqrt( wdat.agg$varCases.pred.1step/vvec.1step^2 )
  wdat.agg$fit.1step[is.na(wdat.agg$se.conf.1step)] <- NA
  wdat.agg$estRate.1step[is.na(wdat.agg$se.conf.1step)] <- NA
  
  wdat.agg$numCases[!wdat.agg$actual] <- NA
  wdat.agg <- wdat.agg[order(wdat.agg$week),]
  attr(wdat.agg, "model") <- attributes(wdat.fit)$model
  return(wdat.agg)
}
