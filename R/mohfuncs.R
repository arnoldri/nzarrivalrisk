# MoH Functions for reading data

#' MoH read daily case file
#' 
#' @param filename Full path to daily case filename
#' 
#' @export
moh_read_daily_cases <- function(filename) {
  read_csv(filename, col_types = cols(
    .default = col_character(),
    ReportDate = col_date(format = ""),
    OnsetDt = col_date(format = ""),
    DtArrived = col_date(format = ""),
    LabDate = col_date(format = ""),
    HospDt = col_date(format = ""),
    DischDt = col_date(format = ""),
    IsolatedFromDate = col_date(format = ""),
    ModifiedDateTime = col_datetime(format = "")
  ))
}

#' MoH read daily arrivals file
#' 
#' @param filename Full path to daily arrivals filename
#' 
#' @export
moh_read_daily_arrivals <- function(filename) {
  read_csv(filename, col_types = cols(
    .default = col_character(),
    `Arrival date` = col_date(format = ""),
    `Report date` = col_date(format = "")
  ))
}

#' MoH Summarise daily arrivals
#' 
#' @param arrivals Arrivals data frame
#' 
#' @export
moh_summarise_daily_arrivals <- function(arrivals) {
  arrivals %>%
    group_by(iso3c = coalesce(`Arrival card iso`, `Route iso`, `EpiSurv iso`, `Travelled from iso`),
             `Arrival date`) %>%
    summarise(numArr = n(),
              numCases = sum(!is.na(EpiSurv)),
              numEligibleArr = sum(`COVID 19 Vaccine Status` == 'Full' | `Under 12` == 'Yes' | `EpiSurv Immunised` == 'Yes', na.rm = T),
              numEligibleCases = sum(`EpiSurv Immunised` == 'Yes' | (!is.na(EpiSurv) & `Under 12` == 'Yes'), na.rm = T),
              numVaccArr = sum(`COVID 19 Vaccine Status` == 'Full' | `EpiSurv Immunised` == 'Yes', na.rm = T),
              numVaccCases = sum(`EpiSurv Immunised` == 'Yes', na.rm = T),
              .groups = 'drop')
}

