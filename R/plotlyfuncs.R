
#' Interactive image style plot of risk classifications by country by week
#' 
#' @param wdat.fit Weekly data with fitted values and forecasts
#' @param type One of 'cases', 'rates', 'cases.conf', 'rates.conf', 
#' 'cases.pred' or 'rates.pred' for cases or rates and fitted values
#' upper bound of the confidence interval or upper bound of the 
#' prediction interval
#' @param upper.conf Size of upper tail excluded by the confidence interval
#' @param upper.pred Size of upper tail excluded by the prediction interval
#' @param subset Logical vector with the same length as the number of rows of \code{wdat.fit}
#' @param order Ordering
#' @param worder Week at which 'order' is to be applied (if NULL: last week of real data)
#' @param cutpoints Cutpoints list object
#' @param date.format Format of dates for the time axis
#' @param xlas Orientation of the date axis labels
#' @param with.legend Logical: draw the legend?
#' @param show.cuts Show the numeric values of the cutpoints in the legend
#' 
#' @details Same as 'plot_risk_classification' only interactive. Use 'htmlwidgets::saveWidget' to save in an HTML file
#' 
#' @export
plotly_risk_classification<-function(wdat.fit, type="cases", upper.conf=0.1, upper.pred=0.1, 
                                     subset=NULL, order="country", worder=NULL,
                                     main=NULL,
                                     cutpoints=list(cases=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                                    rates=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                                    cases.conf=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                                    rates.conf=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf),
                                                    cases.pred=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf), 
                                                    rates.pred=c("Low"=0, "Medium"=3, "High"=8, "Inf"=Inf),
                                                    rate.multiplier=1000),
                                     date.format="%d-%b", xlas=2, with.legend=TRUE, 
                                     upper.axis=FALSE, show.cuts=TRUE
) {
    
    
  if(!is.null(main)) {
    title.str <- NA
  } else if(type=="cases") {
    title.str <- "Cases per week"
  } else if(type=="cases.conf") {
    title.str <- paste0("Cases per week - ",100*(1-upper.conf),"% Confidence")
  } else if(type=="cases.pred") {
    title.str <- paste0("Cases per week - ",100*(1-upper.pred),"% Prediction")
  } else if(type=="rates") {
    title.str <- paste0("Rates per ",cutpoints$rate.multiplier)
  } else if(type=="rates.conf") {
    title.str <- paste0("Rates per ",cutpoints$rate.multiplier," - ",100*(1-upper.conf),"% Confidence")
  } else if(type=="rates.pred") {
    title.str <- paste0("Rates per ",cutpoints$rate.multiplier," - ",100*(1-upper.pred),"% Prediction")
  } else {
    title.str <- ""
  }
  
  if(!is.null(subset)) wdat.fit <- wdat.fit[subset,]
  classvarname <- paste0(type,".class")
  cuts <- cutpoints[[type]]
  cuts <- cuts[-length(cuts)]
  hh <- classify_countries(wdat.fit, 
                           upper.conf=upper.conf, upper.pred=upper.pred,
                           cutpoints=cutpoints)
  hh <- hh[,c("country","week","actual",classvarname)]
  names(hh) <- c("country","week","actual","class")
  hh <- hh[!is.na(hh$class),]
  
  risknames <- names(cutpoints$cases)[-length(cutpoints$cases)]
    riskcols <- set.riskcols(risknames)
     
  
  hh$class.int <- as.numeric(factor(hh$class, levels=risknames))
  cnames <- unique(hh$country)
  wmin <- min(hh$week)
  wmax <- max(hh$week)
  weekvec <- seq(from=wmin, to=wmax)
  
  nc <- length(unique(hh$country))
  nw <- diff(range(hh$week))+1
  hdat <- expand.grid(country=cnames, week=weekvec)
  hdat <- merge(hdat, hh, all.x=TRUE)
  hdat <- hdat[order(hdat$country, hdat$week),]
  
  zmat <- array(hdat$class.int, dim=c(nw,nc))
  dimnames(zmat) <- list(weekvec,cnames)
  wforecast <- 1+max(hdat$week[hdat$actual], na.rm=TRUE) 
  if(is.null(worder)) worder <- wforecast-1
  
  if(order=="country") {
    odx <- rev(order(dimnames(zmat)[[2]]))
    zmat <- zmat[,odx]
  } else if(order=="class") {
    odx <- order(zmat[as.character(worder),])
    zmat <- zmat[,odx]
  } else if(order=="numArr") {
    w1 <- wdat.fit[wdat.fit$week==worder,]
    cc <- rev(w1$country[order(w1$numArr,w1$country,decreasing=c(TRUE,FALSE),method="radix")])
    odx <- order(factor(dimnames(zmat)[[2]],levels=cc))
    zmat <- zmat[,odx]
  } else if(order=="numCases") {
    w1 <- wdat.fit[wdat.fit$week==worder,]
    cc <- rev(w1$country[order(w1$numCases,w1$country,decreasing=c(TRUE,FALSE),method="radix")])
    odx <- order(factor(dimnames(zmat)[[2]],levels=cc))
    zmat <- zmat[,odx]
  } else if(order=="rates") {
    w1 <- wdat.fit[wdat.fit$week==worder,]
    cc <- rev(w1$country[order(w1$numCases/(1e-7+w1$numArr),w1$country,decreasing=c(TRUE,FALSE),method="radix")])
    odx <- order(factor(dimnames(zmat)[[2]],levels=cc))
    zmat <- zmat[,odx]
  } else if(order%in%c("upwards","downwards","static")) {
    week1 <- min(worder,wmax-1)
    week2 <- week1+1
    dd <- zmat[as.character(week2),] - zmat[as.character(week1),]
    if(order=="upwards") {
      zmat <- zmat[,dd>0]
    } else if(order=="downwards") {
      zmat <- zmat[,dd<0]
    } else { # static
      zmat <- zmat[,dd==0]
    }
    z1 <- zmat[as.character(week1),]
    z2 <- zmat[as.character(week2),]
    zcountries <- dimnames(zmat)[[2]]
    if(order=="upwards") {
      cc <- rev(zcountries[order(z1,zcountries,decreasing=c(TRUE,FALSE),method="radix")])
    } else {
      cc <- rev(zcountries[order(z2,zcountries,decreasing=c(TRUE,FALSE),method="radix")])
    }
    odx <- order(factor(zcountries,levels=cc))
    zmat <- zmat[,odx]
  } else {
    invisible()
  }
  
  par(mar=c(5.1,8.1,4.1,2.1))
  xvec <- week.date(as.numeric(dimnames(zmat)[[1]]))
    yvec <- 1:ncol(zmat)
    dd<-expand.grid(date=xvec,from=colnames(zmat))

    if(show.cuts) risknames <- paste0(risknames," (>",cuts,")") 
    dd$risk=factor(as.vector(zmat),labels=risknames)

    
    fig<-ggplot(dd,aes(x=date,y=from,fill=risk))+geom_tile()+scale_fill_discrete(type=riskcols,position="top")+xlab("")+ylab("")

  xp1 <- as.numeric(week.date(wforecast)-3.5)#; xp2 <- par()$usr[2]

    fig<-fig+geom_rect(xmin=xp1,xmax=max(xvec)+4,ymin=0.5,ymax=length(yvec)+0.5, colour="#FFFF00", fill=NA, size=2)
    fig<-fig+geom_vline(xintercept=as.numeric(week.date(worder)+3.5), col="dark blue", size=1, linetype="dashed")

  if(upper.axis) warning("upper axis not supported") #axis.Date(3, at=xvec, format=date.format, las=xlas)

  if(!is.na(title.str)) fig<-fig+ggtitle(paste0("Risk classification (",title.str,")\n"))
  
  ## # add year annotation
  ## ymin <- as.numeric(format(min(xvec),"%Y"))
  ## ymax <- as.numeric(format(max(xvec),"%Y"))
  ## ystarts <- as.Date(paste("01-01-",ymin:ymax), format="%d-%m-%Y")
  ## ystarts[1] <- min(xvec)
  ## ystarts <- as.numeric(ystarts)
  ## mtext(ymin:ymax, side=3, line=0, cex=0.5, adj=0, at=ystarts)
  
  # add legend
    if(!with.legend) {
        fig<-fig+theme(legend.position="none")
   }

    
ggplotly(fig)


    
}
